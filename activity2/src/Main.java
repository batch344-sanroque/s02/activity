import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        int[] primeNumbers = new int[5];

        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        System.out.println("The First Prime Number is: " + primeNumbers[0]);
        System.out.println("The Second Prime Number is: " + primeNumbers[1]);
        System.out.println("The Third Prime Number is: " + primeNumbers[2]);
        System.out.println("The Fourth Prime Number is: " + primeNumbers[3]);
        System.out.println("The Fifth Prime Number is: " + primeNumbers[4]);


        ArrayList<String> friends = new ArrayList<String>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);


        HashMap<String,Integer> inventoryList = new HashMap<String, Integer>();

        inventoryList.put("toothpaste", 15);
        inventoryList.put("toothbrush", 20);
        inventoryList.put("soap", 12);

        System.out.println("Our Current Inventory consists of: " + inventoryList);

    }
}